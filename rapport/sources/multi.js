/*paramètre multijoueur */
var server_addr = "http://127.0.0.1:8000"; //addr du serv multijoueur
var id; //id que va nous attribuer le serv
var player_list; //la liste des joueurs tenue à jour


function init() {
	//init camera
	//init renderer
	//init player
	//calcul du déplacement
	//init futur_player
	addLights();
	//event gestion des touches push pour la gestion des déplacements dans animate
	//GUI

	//demande une id au serv 
	var xhr = new XMLHttpRequest();
	xhr.open('GET', server_addr, false);
	try {
		xhr.send(null);
	}catch(exception) {//pour éviter que les erreurs de requêtes ne bloquent tout le programme
	  	console.log('There was a network error.');
	}
		
	//on extrait l'ID du joueur de la requête
	reponse =  JSON.parse(xhr.response);
	id = reponse.yourId;
	
	update_position(true);//on lance une requête synchrone pour remplir le tableau de joueurs
	update_position(false);//on lance la routine de mise à jour des positions serveur (asynchrone)
}


function animate() {
	show_other_player(); //fonction d'affichage des autres joueurs
	deplacement();//mise à jour des déplacements / rotations
	control_time.setValue(Math.round((Date.now()-start_time)/10)/100);
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}


function update_position(start){ //start false => requête asynchrone et lancement de routine
	var xhr = new XMLHttpRequest();
	xhr.open('POST', server_addr, !start);//asynchrone pour éviter de bloquer le programme
	xhr.onreadystatechange = function() {//quand la requête est finie
		 if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
			player_list =  JSON.parse(xhr.response);//mise à jour de la liste des utilisateurs		
			if(!start){
				update_position();//une fois la requête terminée et traitée, on en relance une	
			}		
		}
	}
	xhr.send(JSON.stringify({"id": id, "x": player.position.x, "y":player.rotation.y, "z":player.position.z}));//paramètre à envoyer au server	
}



function show_other_player(){
	// supprime les modèles de tous les joueurs
	for( i in player_model_list){
		scene.remove(player_model_list[i]);
	}
	// ajoute de nouveaux modèles pour chaques joueurs transmis par le serveur
	player_model_list = [];
	for (e in player_list["users"]){
		if(e != id){// affiche qu'une fois notre voiture
			player_model_list[e] = player.clone();// fait un clone de notre voiture
			player_model_list[e].position.x = player_list["users"][e]["x"];
			player_model_list[e].rotation.y = player_list["users"][e]["y"];
			player_model_list[e].position.z = player_list["users"][e]["z"];
			scene.add(player_model_list[e]);
		}				
	}	
}

