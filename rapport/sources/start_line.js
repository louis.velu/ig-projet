function createGround() {
	// Create ground geometry and material
	//create a triangular geometry
	for(var k=0;k<2*gsize/UNITWIDTH;k++){
		for(var i=0;i<2*gsize/UNITWIDTH;i++){
			// Determine the Color to put in the Grid
			var c;
			if(i>=2*gsize/UNITWIDTH/10*2 && i<2*gsize/UNITWIDTH/10*8 && k>=2*gsize/UNITWIDTH/10*2 && k<2*gsize/UNITWIDTH/10*8){//carré au centre 
					//dans le cas d'une case bleue
					c=new THREE.Color(0.0,0.0,1.0)
			//Dans le cas du damier
			}else if((i+k)%2==0){
				c=new THREE.Color(0.0,0.05,0.05);
			}else{
				c=new THREE.Color(0.0,0.0,0.0);
			}
			// Draw the First Triangle with the color				
			//add the face to the geometry's faces array
			// Draw the Second Triangle with the same color
			//add the face to the geometry's faces array
		}
	}

	//the face normals and vertex normals can be calculated automatically
	var groundMat = new THREE.ShaderMaterial( {
        	vertexShader: document.getElementById( 'vertexShader' ).textContent,
        	fragmentShader: document.getElementById( 'fragmentShader' ).textContent
        });
	//ajout (groundgeo, groundMat) à la scène
	/**ligne de départ**/
	var start_line_geo = new THREE.Geometry();
	var c_blanc = new THREE.Color(1.0,1.0,1.0);
	for(var i=0;i<2*gsize/10*2-UNITWIDTH;i++){
		var v1,v2,v3;
		var v4,v5,v6;
		var f1,f2;
		if(i%2 == 0){//si pair on fait une case blanche en haut
			v1 = new THREE.Vector3(i-gsize,0.01,0);
			v2 = new THREE.Vector3(i-gsize,0.01,1);
			v3 = new THREE.Vector3(i-gsize+1,0.01,1);
			v4 = new THREE.Vector3(i-gsize+1,0.01,0);
			f1 = new THREE.Face3(4*i,4*i+1,4*i+2);
			f2 = new THREE.Face3(4*i,4*i+2,4*i+3);	
		}else{//si impair on fait une case blanche en bas
			v1 = new THREE.Vector3(i-gsize+,0.01,0);
			v4 = new THREE.Vector3(i-gsize+1,0.01,-1);
			v2 = new THREE.Vector3(i-gsize,0.01,-1);
			v3 = new THREE.Vector3(i-gsize+1,0.01,0);
			f1 = new THREE.Face3(4*i+1,4*i,4*i+3);
			f2 = new THREE.Face3(4*i,4*i+2,4*i+3);	
		}
		start_line_geo.vertices.push(v1,v2,v3,v4);
		f1.vertexColors[0]=c_blanc;
		f1.vertexColors[1]=c_blanc;
		f1.vertexColors[2]=c_blanc;
		f2.vertexColors[0]=c_blanc;
		f2.vertexColors[1]=c_blanc;
		f2.vertexColors[2]=c_blanc;
		start_line_geo.faces.push(f1);
		start_line_geo.faces.push(f2);
	}
	var start_line = new THREE.Mesh(start_line_geo,groundMat);
	scene.add(start_line);
}

