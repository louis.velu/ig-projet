//GUI initialisé dans la fonction INIT
var gui = new dat.GUI();
var start_time; //va contenir la valeur du temps au début
var control_dist; //contrôleur gui de distance
var control_tour; //contrôleur gui des tours
var control_time; //contrôleur gui du temps écoulé depuis le début

function init() {
	//initialisation de la scène
	//initialisation de la caméra
	//initialisation du renderer
	//initialisation du player
	//et du player_futur pour le calcul de déplacement
	//ajout de la grille		
	addLights();
	//event keyup et keydown

	//GUI
	var init_data = {distance: 0, tour: 'start', temps: 'start'};//va contenir la distance parcourue
	control_dist = gui.add(init_data, 'distance').listen();
	control_tour = gui.add(init_data, 'tour').listen();
	//ajoute le temps à GUI
	control_time = gui.add(init_data, 'temps').listen();
	start_time = Date.now();
		

}

function animate() {
	deplacement();//mise a jour des déplacements / rotations
	control_time.setValue(Math.floor((Date.now()-start_time)/10)/100);
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
};
