function init() {
	//initialisation de la scène
	//initialisation de la caméra
	//initialisation du renderer
		
	player = new THREE.Object3D();
	var box1_geometry = new THREE.BoxGeometry(2,2,5);
	var box1_material = new THREE.MeshBasicMaterial( { color: 0xf50505 } );
	var box = new THREE.Mesh( box1_geometry, box1_material );
	box.position.y = 1.25;
	player.add(box);
	var roue_geometry = new THREE.CylinderGeometry(0.5,0.5,0.2);
	var roue_material = new THREE.MeshBasicMaterial( { color: 0x050505 } );
	var roue1 = new THREE.Mesh( roue_geometry, roue_material );
	roue1.position.x = -0.9;
	roue1.position.z = -2.25;
	roue1.rotation.z = THREE.Math.degToRad(90);
	var roue2 = new THREE.Mesh( roue_geometry, roue_material );
	roue2.position.x = 0.9;
	roue2.position.z = -2.25;
	roue2.rotation.z = THREE.Math.degToRad(90);	
	var roue3 = new THREE.Mesh( roue_geometry, roue_material );
	roue3.position.x = -0.9;
	roue3.position.z = 2.25;
	roue3.rotation.z = THREE.Math.degToRad(90);
	var roue4 = new THREE.Mesh( roue_geometry, roue_material );
	roue4.position.x = 0.9;
	roue4.position.z = 2.25;
	roue4.rotation.z = THREE.Math.degToRad(90);
	player.add(roue1);
	player.add(roue2);
	player.add(roue3);
	player.add(roue4);
		
	scene.add( player );

	player.position.x = -40;
	player.position.z = 10;
	player.position.y = 0.5;

	//initialisation du player_futur pour le calcul de déplacement	
	addLights();
	//event keyup et keydown

	//initialisation des variables GUI

}
