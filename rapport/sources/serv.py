#!/usr/bin/python
#prévu pour python3
#sources insipirées de https://gist.github.com/bsingr/a5ef6834524e82270154a9a72950c842

from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
import json
import time

class LocalData(object):
	last_id = 0
	user_list = {};#liste des joueurs
	time_out = 1.500;#temps sans requête d'un utilisateur avant de le considéré déconnecté

class RequestHandler(BaseHTTPRequestHandler):	
	
	def do_GET(self):#requête avec la méthode GET, on attribue un ID à l'utilisateur
		parsed_path = urlparse(self.path)
		self.send_response(200)#code http good request
		self.send_header("Access-Control-Allow-Origin", "*")#contrôle de l'accès ouvert
		self.send_header("Content-Type", "application/json")#type de retour json
		self.end_headers()
		self.wfile.write(json.dumps({
			'yourId': LocalData.last_id+1#attribution de l'ID
		}).encode())
		LocalData.last_id+=1
		return

	def do_POST(self):#requête avec la méthode POST, l'utilisateur envoie son id et ses coordonnées, on lui répond les coordonnées de tous les joueurs

		content_len = int(self.headers.get('content-length'))
		post_body = self.rfile.read(content_len)
		data = json.loads(post_body)
		LocalData.user_list[data["id"]] = {"x":data["x"], "y":data["y"], "z":data["z"], "time": time.time()}#on met à jour les coordonnées ou on ajoute l'utilisateur s'il n'existe pas
		#on retire les joueurs time out
		old_list = LocalData.user_list
		LocalData.user_list = {}
		for e in old_list:
			if old_list[e]["time"] > time.time()-LocalData.time_out:
				LocalData.user_list[e] = old_list[e]

		parsed_path = urlparse(self.path)
		self.send_response(200)#code http good request
		self.send_header("Access-Control-Allow-Origin", "*")#contrôle de l'accès ouvert
		self.send_header("Content-Type", "application/json")#type de retour json
		self.end_headers()
		self.wfile.write(json.dumps({
			'users' : LocalData.user_list#on envoie les coordonnées de tous les joueurs
		}).encode())
		return

if __name__ == '__main__':
	server = HTTPServer(('localhost', 8000), RequestHandler)
	print('Starting server at http://localhost:8000')
	server.serve_forever()
