var boost_time = 500; //3sec
var boost_date; //date du passage sur la zone de boost
var boost_effect = 0.5;
var boost_patern_size = 4;
var boost_patern_nb = 3;
var boost_partern_color = new THREE.Color(1.0,0.01,0.03);



function createGround() {
	/**damier avec zone bleu et verte**/
	/**ligne de départ**/
	/**zone de boost**/
	var patern_geo = new THREE.Geometry();
	patern_geo.vertices.push(
		new THREE.Vector3(-boost_patern_size/2, 0, 0),			//1: -1,0
		new THREE.Vector3(0, 0, -boost_patern_size/2),			//2: 0,-1
		new THREE.Vector3(-boost_patern_size, 0, -boost_patern_size/2),	//3: -2,-1
		new THREE.Vector3(-boost_patern_size/2, 0, -boost_patern_size/2),//4: -1,-1
		new THREE.Vector3(0, 0, -boost_patern_size),			//5: 0,-2
		new THREE.Vector3(-boost_patern_size, 0, -boost_patern_size)	//6: -2,-2
	);
	//on ajoute les 3 triangles
	var f1 = new THREE.Face3( 0,1,2);
	var f2 = new THREE.Face3( 1,4,3);
	var f3 = new THREE.Face3( 2,3,5);
	//on ajoute les couleurs a chaque points
	f1.vertexColors = [boost_partern_color, boost_partern_color, boost_partern_color];
	f2.vertexColors = [boost_partern_color, boost_partern_color, boost_partern_color];
	f3.vertexColors = [boost_partern_color, boost_partern_color, boost_partern_color];
	patern_geo.faces.push(f1, f2, f3);
	patern_mat = new THREE.ShaderMaterial({
	            	vertexShader: document.getElementById( 'vertexShader' ).textContent,
	            	fragmentShader: document.getElementById( 'fragmentShader' ).textContent
	});
	patern = new THREE.Mesh(patern_geo, patern_mat);
	for(var i=0; i<boost_patern_nb; i++){
		var patern_cloned = patern.clone();
		patern_cloned.position.x = gsize-2*gsize/10*2+boost_patern_size;
		patern_cloned.position.z = gsize-2*gsize/10*7+(i+1)*boost_patern_size;
		patern_cloned.position.y = 0.001;
		scene.add(patern_cloned);
	}
}

function deplacement(){
	//calcul de la vitesse par l'acceleration
	
	//première estimation de la position pour le boost
	futur_player.position.x = player.position.x;
	futur_player.position.z = player.position.z;
	futur_player.rotation.y = player.rotation.y;
	futur_player.translateZ(-vitesse);
	if(	//dans la zone du boost
		player.position.x < gsize-2*gsize/10*2+boost_patern_size &&
		player.position.x > gsize-2*gsize/10*2 &&
		player.position.z < gsize-2*gsize/10*7+(boost_patern_nb+1)*boost_patern_size &&
		player.position.z > gsize-2*gsize/10*7+boost_patern_size &&
		//dans le bon sens
		player.position.z < futur_player.position.z
	){
		boost_date = Date.now();
	}

	var r_vitesse = vitesse;

	if(boost_date >= Date.now()-boost_time){//si le boost fait éffet
		r_vitesse = r_vitesse + boost_effect;
	}	
	//diminution de la vitesse dans la zone verte
	//on applique la vitesse au joueur 
	// RIGHT
	// LEFT
}
