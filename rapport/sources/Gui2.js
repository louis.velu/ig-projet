//GUI initialisé dans la fonction INIT
var gui = new dat.GUI();
var control_dist; //contrôleur gui de distance
var control_tour; //contrôleur gui des tours

function init() {
	//initialisation de la scène
	//initialisation de la caméra
	//initialisation du renderer
	//initialisation du player
	player.position.x = -40;
	player.position.z = 10;
	player.position.y = 0.5;
	//et du player_futur pour le calcul de déplacement
	//ajout de la grille		
	addLights();
	//event keyup et keydown

	//GUI
	var init_data = {distance: 0, tour: 'start'};//va contenir la distance parcourue
	control_dist = gui.add(init_data, 'distance').listen();
	//ajoute le nombre de tour à GUI
	control_tour = gui.add(init_data, 'tour').listen();

}

function deplacement(){
	vitesse = zSpeed;
	// UP
	if (key_list[38]) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(-vitesse);
		if(!(/*position hors du damier */
			/*position dans la zone bleue*/
		)){//si position permise
			if(
				player.position.x < 0/*position comprise dans [-50, -30] */
				&& player.position.z > 0/* le joueur n'a pas passé la ligne de départ */			
				&& futur_player.position.z < 0/* il va passer la ligne de départ*/
			){
				tour = control_tour.getValue();
				if(tour == 'start'){//si la partie n'a pas encore commencé
					tour = 0;
				}else{
					tour++;
				}
					control_tour.setValue(tour);//mise à jour du nombre de tour
			}else if(
				player.position.x < -gsize*2/10*2/*position comprise dans [-50, -30] */				
				&& player.position.z < 0
				&& futur_player.position.z > 0
			){//la ligne est passée dans l'autre sens!!
				tour = control_tour.getValue();
				if(tour == 'start' || tour == 0){//si la partie n'a pas encore commencée ou tour 0						
					tour = 'start';
				}else{
					tour--;
				}
				control_tour.setValue(tour);//mise à jour du nombre de tour
			}
			player.translateZ(-vitesse);
			control_dist.setValue(control_dist.getValue()+vitesse);//mise à jour de la distance parcourue
		}
	} 
	// DOWN
	if (key_list[40]) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(vitesse/2);
		if(!(/*position hors du damier */
			/*position dans la zone bleue*/
		)){//si position permise
			if(
				player.position.x < -gsize*2/10*2/*position comprise dans [-50, -30] */
				&& player.position.z > 0/* le joueur n'a pas passé la ligne de départ */
				&& futur_player.position.z < 0/* il va passer la ligne de départ*/){

				if(tour == 'start'){//si la partie n'a pas encore commencée
					tour = 0;
				}else{
					tour++;
				}
			}else if(
				player.position.x < -gsize*2/10*2/*position comprise dans [-50, -30] */
				&& player.position.z < 0
				&& futur_player.position.z > 0){//la ligne est passée dans l'autre sens!!
				if(tour == 'start' || tour == 0){//si la partie n'a pas encore commencé ou tour 0
					tour = 'start';
				}else{
					tour--;
				}
			}
				player.translateZ(vitesse/2);
		 }
	} 
	// RIGHT
	// LEFT
}
