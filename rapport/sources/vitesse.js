var vitesse = 0;
var deceleration_naturel = 0.02;
var acceleration = 0.015;
var frein = 0.07; //taux de freinage
var vitesse_max = 1.3;//vitesse max en marche avant
var vitesse_min = -0.5;//vitesse max en marche arrière
function deplacement(){
	var r_acceleration = acceleration;
	if(	//si dans la zone verte
		player.position.x<-gsize+UNITWIDTH ||
		player.position.z<-gsize+UNITWIDTH ||
		player.position.x>=gsize-UNITWIDTH ||
		player.position.z>=gsize-UNITWIDTH
	){
		r_acceleration = acceleration/2;
	}
	// UP accélération (ou frein si marche arrière)
	if (key_list[38]) {
		if(vitesse >= 0){//si on est en marche avant ou à l'arrêt
			vitesse = Math.min(vitesse+r_acceleration,vitesse_max);
		}else if(!key_list[40]){//si marche arrière et pas d'accélération
			vitesse = Math.min(0, vitesse+frein);
		}	
	}

	// DOWN décélération (ou accélération si marche arrière)
	if (key_list[40]) {
		if(vitesse <= 0){//si marche arrière ou à l'arrêt
			vitesse = Math.max(vitesse_min, vitesse-r_acceleration/2);
		}else if(!key_list[38]){//si marche avant et pas d'accélération
			vitesse = Math.max(0, vitesse-frein);
		}
	} 

	if(!key_list[38] && !key_list[40]){//si on ne fait rien
		//décélération naturelle	
		if(vitesse > 0){//si marche avant
			vitesse = Math.max(0, vitesse-deceleration_naturel);//pour éviter de passer en négatif
		}else if(vitesse < 0){//si marche arrière
			vitesse = Math.min(0, vitesse+deceleration_naturel);//pour éviter de passer en négatif		
		}
	}
	var r_vitesse = vitesse;
	if(	//si dans la zone verte
		player.position.x<-gsize+UNITWIDTH ||
		player.position.z<-gsize+UNITWIDTH ||
		player.position.x>=gsize-UNITWIDTH ||
		player.position.z>=gsize-UNITWIDTH
	){
		r_vitesse = r_vitesse/2;	//vitesse diminuée		
	}
	//on applique la vitesse
	if(r_vitesse != null && r_vitesse != 0){
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(-r_vitesse);
		if(!(/*position hors du damier */
			/*position dans la zone bleue*/
		){//si position permise
			//mise à jour variable de tour Gui
			player.translateZ(-r_vitesse);
			//mise à jour variable distance parcourue Gui
		}else{//on est contre un objet, notre vitesse passe à 0
			vitesse = 0;
		}
	}
	if(r_vitesse > 0){
		control_dist.setValue(control_dist.getValue()+r_vitesse);//mise à jour de la distance parcourue
	}
	// RIGHT
	// LEFT

}
