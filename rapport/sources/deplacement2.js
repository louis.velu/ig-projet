function deplacement(){
	vitesse = zSpeed;
	// UP
	if (keyCode == 38) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(-vitesse);
		if(!(/*position hors du damier */
			futur_player.position.z < -gsize ||
			futur_player.position.x > gsize || 
			futur_player.position.z > gsize || 
			futur_player.position.x < -gsize || 
			/*position dans la zone bleue*/
			(futur_player.position.z<gsize-2*2*gsize/10 &&
			 futur_player.position.z>gsize-2*gsize/10*8 &&
			 futur_player.position.x<gsize-2*2*gsize/10 &&
			 futur_player.position.x>gsize-2*gsize/10*8))
		){//si position permise
			player.translateZ(-vitesse);
		}
	} 
	// DOWN
	if (keyCode == 40) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(vitesse/2);
		if(!(/*position hors du damier */
			futur_player.position.z < -gsize ||
			futur_player.position.x > gsize || 
			futur_player.position.z > gsize || 
			futur_player.position.x < -gsize || 
			/*position dans la zone bleue*/
			(futur_player.position.z<gsize-2*2*gsize/10 &&
			 futur_player.position.z>gsize-2*gsize/10*8 &&
			 futur_player.position.x<gsize-2*2*gsize/10 &&
			 futur_player.position.x>gsize-2*gsize/10*8))
		){//si position permise
			player.translateZ(vitesse);
		}
	} 
	// RIGHT
	// LEFT
}
