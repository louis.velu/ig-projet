//on a diminué ces valeurs
var zSpeed=0.75;
var xSpeed=0.25;

var key_list = {}; //liste des touches encore push

function init() {
	//initialisation de la scène
	//initialisation de la caméra
	//initialisation du renderer
	//initialisation du player
	//et du player_futur pour le calcul de déplacement
	//ajout de la grille		
	addLights();

	//event gestion des touches push pour la gestion des déplacements dans animate
	document.addEventListener("keydown", onDocumentKeyDown, false);
	document.addEventListener("keyup", onDocumentKeyUp, false);
}

function animate() {
	deplacement();//mise à jour des déplacements / rotations
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}

function deplacement(){
	vitesse = zSpeed;
	// UP
	if (key_list[38]) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(-vitesse);
		if(!(/*position hors du damier */
			/*position dans la zone bleue*/
		){//si position permise
			player.translateZ(-vitesse);
		}
	} 
	// DOWN
	if (key_list[40]) {
		futur_player.position.x = player.position.x;
		futur_player.position.z = player.position.z;
		futur_player.rotation.y = player.rotation.y;
		futur_player.translateZ(vitesse/2);
		if(!(/*position hors du damier */
			/*position dans la zone bleue*/
		){//si position permise
			player.translateZ(vitesse);
		 }
	} 
	// RIGHT
	if (key_list[39]) {
		player.rotation.y -= xSpeed/6;
	} 
	// LEFT
	if (key_list[37]) {UNITWIDTH
		player.rotation.y += xSpeed/6;
	}
}


function onDocumentKeyUp(event){
	key_list[event.keyCode] = false;//on met à jour la liste des touches maintenues
}

function onDocumentKeyDown(event) {
	key_list[event.keyCode] = true;//on met à jour la liste des touches maintenues
	// A, camera in First Person Mode or Behind
	//on laisse cette fonction ici pour éviter de changer trop vite de mode dans le animate
	if (key_list[65]) {
		if(FP)
		// CAMERA BEHIND
		{
			camera.position.z = 50;
			camera.position.y = 10;
		}
		// FPS CAMERA
		else{
			camera.position.z = -2;
			camera.position.y = 3;
		}
		FP=!FP;
	}
}
