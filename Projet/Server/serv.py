#!/usr/bin/python
#prevu pour python3
#Louis Vélu
#Serveur - Projet
#sources insipire de https://gist.github.com/bsingr/a5ef6834524e82270154a9a72950c842

#curl --data "{\"id\": 1, \"x\": 15, \"y\":0.5, \"z\":5.2}" --header "Content-Type: application/json" http://localhost:8000


from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
import json
import time

#serv_addr = "localhost"
serv_addr = "192.168.1.68"
class LocalData(object):
	last_id = 0
	user_list = {};#liste des joueurs
	time_out = 1.500;#temps sans requête d'un utilisateur avant de le concidéré déconnecté

class RequestHandler(BaseHTTPRequestHandler):	
	
	def do_GET(self):#requête avec la méthode GET, on attribut un ID a l'utilisateur
		parsed_path = urlparse(self.path)
		self.send_response(200)
		self.send_header("Access-Control-Allow-Origin", "*")
		self.send_header("Content-Type", "application/json")
		self.end_headers()
		self.wfile.write(json.dumps({
			'yourId': LocalData.last_id+1#attribution de l'ID
		}).encode())
		LocalData.last_id+=1
		return

	def do_POST(self):#requête avec la méthode POST, l'utilisateur envoi son id et ces coordonnée, on lui envoie les coordonnée de tout les joueurs

		content_len = int(self.headers.get('content-length'))
		post_body = self.rfile.read(content_len)
		data = json.loads(post_body)
		LocalData.user_list[data["id"]] = {"x":data["x"], "y":data["y"], "z":data["z"], "time": time.time()}#on met a jour les coordonnée / ou on ajoute l'utilisateur si il n'existe pas
		#on retir les joueurs time out
		old_list = LocalData.user_list
		LocalData.user_list = {}
		for e in old_list:
			if old_list[e]["time"] > time.time()-LocalData.time_out:
				LocalData.user_list[e] = old_list[e]

		parsed_path = urlparse(self.path)
		self.send_response(200)
		self.send_header("Access-Control-Allow-Origin", "*")
		self.send_header("Content-Type", "application/json")
		self.end_headers()
		self.wfile.write(json.dumps({
			'users' : LocalData.user_list#on envoie les coordonnée de tout les joueurs
		}).encode())
		return

if __name__ == '__main__':
	server = HTTPServer((serv_addr, 8000), RequestHandler)
	print('Starting server at http://'+serv_addr+':8000')
	server.serve_forever()
